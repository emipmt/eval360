cd dev
echo "etiqueta para commit $1"
echo "Compilando proyectos: "
getLocation= dir
echo $getLocation 

cd admin 
echo "\e[1;34mCompilando admin ...\e[0m"
 npm run build
cd ../panel
echo "\e[1;34mCompilando panel ...\e[0m"
 npm run build
cd ../user
echo "\e[1;34mCompilando user ...\e[0m"
 npm run build

cd ../../

git add .
git commit -m "[AUTOPUSH] $1"
git push origin master

exit
