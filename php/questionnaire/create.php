<?php

require_once '../conn.php';

$user = $_POST['user'];
$title = $_POST['title'];
$name = $_POST['name'];
$area = $_POST['area'];
$stall = $_POST['stall'];
$relation = $_POST['relation'];
$email = $_POST['email'];
$suggestion = $_POST['suggestion'];
$frequency = $_POST["frequency"];
$qualification = $_POST["qualification"];
$numberOfParticipants = $_POST['numberOfParticipants'];
$campain = $_POST['campain'];
$aspects = json_encode(json_decode($_POST['aspects']), JSON_UNESCAPED_UNICODE);
$reactivesAnswers = json_encode(json_decode($_POST['reactivesAnswers']), JSON_UNESCAPED_UNICODE);

$sqlCreateQuestionnaire = "INSERT INTO questionnaires (user,campain,name,area,stall,relation,email,suggestion,aspects,reactivesAnswers, frequency, qualification)
  VALUES ('{$user}','{$title}','{$name}','{$area}','{$stall}','{$relation}','{$email}','{$suggestion}','{$aspects}','{$reactivesAnswers}', '{$frequency}', '{$qualification}')";
if (mysqli_query($conn, $sqlCreateQuestionnaire)) {
    $response->status = true;
    $response->message = "¡Has concluído el cuestionario!";
    echo json_encode($response);
} else {
    $response->status = false;
    $response->message = "¡Ha ocurrido un error al guardar tu cuestionario!";
    $response->sql = $sqlCreateQuestionnaire;
    $response->aspects = $aspects;
    echo json_encode($response);
}
