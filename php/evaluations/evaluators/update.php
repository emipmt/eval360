<?php 

    include_once "../../conn.php";
    
    $uid = $_POST['uid'];
    $evaluators = $_POST['evaluators'];
    $sqlReadEvaluators = "UPDATE evaluators SET evaluators = '{$evaluators}' WHERE uid = '{$uid}' ";
    if(mysqli_query($conn, $sqlReadEvaluators)){
        $response->status = true;
        $response->message = "Se ha actualizado la lista de evaluadores";
        echo json_encode($response);
    } else {
        $response->status = false;
        $response->message = "No fue posible actualizar la lista de evaluadores, intentalo de nuevo más tarde";
        echo json_encode($response);
    }