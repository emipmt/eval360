<?php

include_once "../../conn.php";

$uid = $_GET['uid'];

$sqlReadEvaluators = "SELECT evaluators FROM evaluators WHERE uid = '{$uid}' ";
$resultReadEvaluators = mysqli_query($conn, $sqlReadEvaluators);

if (mysqli_num_rows($resultReadEvaluators) > 0) {

    $assoc = mysqli_fetch_assoc($resultReadEvaluators);
    $rowReadEvaluators = json_decode($assoc['evaluators'], true);
    $rowquestionnaire = array();

    foreach ($rowReadEvaluators as $row) {

        $sqlquestionnaire = "SELECT reactivesAnswers, date, suggestion, frequency, qualification FROM questionnaires WHERE name = '{$row['name']}' AND email = '{$row['email']}' ";
        $resultquestionnaire = mysqli_query($conn, $sqlquestionnaire);
        $theRow = $row;
        $theRow['questionnaire'] = mysqli_fetch_assoc($resultquestionnaire);

        array_push($rowquestionnaire, $theRow);
    }
    unset($row);

    $response->status = true;
    $response->evaluators = $rowquestionnaire;
    $response->rowReadEvaluators = $rowReadEvaluators;
    echo json_encode($response);
} else {
    $response->status = false;
    $response->message = "No hay evaluadores";
    echo json_encode($response);
}
