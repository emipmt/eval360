<?php

require_once '../conn.php';

$user = $_POST['user'];
$name = $_POST['name'];
$stall = $_POST['stall'];
$area = $_POST['area'];
$email = $_POST['email'];
$evaluators = $_POST['evaluators'];

$md5 = md5($user . 'secretcodepez' . mb_strtolower($name) . date());

$sqlFindCampains = "SELECT campains FROM users WHERE id = '{$user}' ";
$resultFindCampains = mysqli_query($conn, $sqlFindCampains);

if (mysqli_num_rows($resultFindCampains) > 0) {
    $rowFindCampains = mysqli_fetch_assoc($resultFindCampains);
    $campains = json_decode($rowFindCampains['campains']);

    array_push($campains, [
        "id" => $md5,
        "name" => $name,
        "stall" => $stall,
        "area" => $area,
        "email" => $email,
        "status" => false,
    ]);

    $campains = json_encode($campains, JSON_UNESCAPED_UNICODE);

    $sqlUpdateCampains = "UPDATE users SET campains='{$campains}' WHERE id = '{$user}' ";
    if (mysqli_query($conn, $sqlUpdateCampains)) {
        $sqlCreateTable = "CREATE TABLE reactives_{$md5} LIKE reactives";
        if (mysqli_query($conn, $sqlCreateTable)) {

            $sqlInsertDataToNewTable = "INSERT INTO reactives_{$md5} SELECT * FROM reactives";
            if (mysqli_query($conn, $sqlInsertDataToNewTable)) {
                if(insertEvaluators()){
                    $response->status = true;
                    $response->message = "Se ha creado la evaluación con exito";
                    $response->user = $user;
                    echo json_encode($response);
                }else {
                    $response->status = false;
                    $response->message = "Ha ocurrido un error al ingresar a los evaluadores";
                    echo json_encode($response);
                }
                
            } else {
                $response->status = false;
                $response->message = "No pudo ingresar los datos, intentelo de nuevo más tarde";
                echo json_encode($response);
            }

        } else {
            $response->status = false;
            $response->message = "No pudo clonar el cuestionario, intentelo de nuevo más tarde";
            echo json_encode($response);
        }
    } else {
        $response->status = false;
        $response->message = "No se pudo actualizar el listado de evaluaciones";
        echo json_encode($response);
    }
} else {
    $response->status = false;
    $response->message = "No existe este usuario";
    echo json_encode($response);
}



function insertEvaluators() {

    global $conn;
    global $response;
    global $md5;
    global $user;
    global $evaluators;
        
    $insert = "INSERT INTO evaluators (uid,evaluators) values ('{$md5}','{$evaluators}')";
    if (mysqli_query($conn, $insert)) {
       return true;
    } else {
        return false;
    }
 
}