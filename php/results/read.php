<?php

require_once '../conn.php';

$userId = $_GET["userId"];
$evaluatedId = $_GET["evaluatedId"];

$user;
$campain;

$sqlUser = "SELECT * FROM users WHERE id='{$userId}' ";
$resultUser = mysqli_query($conn, $sqlUser);

if (mysqli_num_rows($resultUser) > 0) {

    $user = mysqli_fetch_assoc($resultUser);
    $campains = json_decode($user['campains']);

    $campainKey = array_search($evaluatedId, array_column($campains, 'id'));

    $campain = $campains[$campainKey];

    $campainName = strtolower($campain->name);

    $sqlQuestionnaires = "SELECT * FROM questionnaires WHERE user='{$userId}' AND campain='{$campainName}' ";
    $resultQuestionnaires = mysqli_query($conn, $sqlQuestionnaires);

    if (mysqli_num_rows($resultQuestionnaires) > 0) {

        $questionnaires = array();

        while ($row = mysqli_fetch_assoc($resultQuestionnaires)) {
            array_push($questionnaires, $row);
        }

        $response->status = true;
        $response->user = $user;
        $response->campain = $campain;
        $response->questionnaires = $questionnaires;
        echo json_encode($response);
    }
} else {
    $response->status = false;
    $response->message = "No se ha encontrado al usuario";
    $response->sql = $sqlUser;
    echo json_encode($response);
}

// $category = $_GET['category'];
// $campain = $_GET['campain'];
// $user = $_GET['user'];

// $sql = "SELECT * FROM questionnaires WHERE user = '{$user}' AND campain = '{$campain}'";
// $result = mysqli_query($conn, $sql);

// if (mysqli_num_rows($result) > 0) {
//     $questionnaires = array();
//     while ($row = mysqli_fetch_assoc($result)) {
//         array_push($questionnaires, $row);
//     }
//     $response->status = true;
//     $response->questionnaires = $questionnaires;
//     echo json_encode($response);
// } else {
//     $response->status = false;
//     $response->message = "Este diagnóstico aún no recibe respuestas";
//     $response->sql = $sql;
//     echo json_encode($response);
// }
