module.exports = {
  transpileDependencies: [
    'vuetify'
  ],
  lintOnSave: false,
  //prod
  publicPath: './results/'
  //dev
  // publicPath: './'
}
