import Vue from 'vue'
import VueRouter from 'vue-router'
import Result from '../views/Result.vue'
import ResultGlobal from '../views/ResultGlobal.vue'

Vue.use(VueRouter)

const routes = [{
  path: '/participantes/:userId/:evaluatedId/',
  name: 'participantes',
  component: Result
},
{
  path: '/global/:userId/:evaluatedId/',
  name: 'global',
  component: ResultGlobal
},

]

const router = new VueRouter({
  routes
})

export default router