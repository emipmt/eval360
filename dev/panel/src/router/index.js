import Vue from 'vue'
import Router from 'vue-router'
import home from '@/components/home'
import Monitoring from '@/components/monitoring/Monitoring.vue'
Vue.use(Router)

export default new Router({
  routes: [{
    path: '/monitoreo/:user/:campain/:uid',
    name: 'Monitoring',
    component: Monitoring
  },
  {
    path: '/',
    name: 'home',
    component: home
  }
  ]
})