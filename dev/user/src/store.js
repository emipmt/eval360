import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    user: {},
	evaluator: {},
	evaluators : [],
    thisCampain: {},
    frequency: "",
    qualification: "",
    reactivesLength: 0,

    calculatedAspects: {},
    reactivesAnswers:{}


  },
  mutations: {
    updateEvaluator(state, evaluator) {
      state.evaluator = evaluator
	},
	updateEvaluators(state, evaluators){
		state.evaluators = evaluators
	},
    updateThisCampain(state, thisCampain) {
      state.thisCampain = thisCampain
    },
    updateFrequency(state, frequency) {
      state.frequency = frequency
    },
    updateQualification(state, qualification) {
      state.qualification = qualification
    },
    updateReactivesLength(state, reactivesLength) {
      state.reactivesLength = reactivesLength
    },
    updateCalculatedAspects(state, calculatedAspects) {
      state.calculatedAspects = calculatedAspects
    },
    updateReactivesAnswers(state, reactivesAnswers) {
      state.reactivesAnswers = reactivesAnswers
    },
  }
})
